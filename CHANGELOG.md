# CHANGELOG

## [Unreleased]

## [v5.1.0]

### Added
- 添加保存文档至内存的功能，Go 项目集成更加方便；

## [v5.0.0]

### Changed
- 改为 XML 作为文档的格式；
